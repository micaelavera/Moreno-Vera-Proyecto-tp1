
--TRAZA 1--
/*En esta traza es mejor la política LRU*/
/*
select get_disk_page (100);
select get_disk_page (101);
select get_disk_page (100);
select get_disk_page (103);
select get_disk_page (101);
select get_disk_page (102);
select get_disk_page (105);
select get_disk_page (102);
select get_disk_page (103);
select get_disk_page (105);
select get_disk_page (103);
*/

--TRAZA 2-- 
/*En esta traza es mejor la política MRU */
/*
select get_disk_page (100);
select get_disk_page (102);
select get_disk_page (104);
select get_disk_page (105);
select get_disk_page (106);
select get_disk_page (106);
select get_disk_page (100);
select get_disk_page (104);
select get_disk_page (107);
select get_disk_page (100);
select get_disk_page (106);
select get_disk_page (107);
select get_disk_page (102);
select get_disk_page (102);
select get_disk_page (100);
*/

--TRAZA 3--
/*En esta traza es mejor la política 139*/
/*
select get_disk_page (10);
select get_disk_page (15);
select get_disk_page (17);
select get_disk_page (12);
select get_disk_page (11);
select get_disk_page (17);
select get_disk_page (9);
select get_disk_page (17);
select get_disk_page (12);
select get_disk_page (11);
select get_disk_page (10);
select get_disk_page (9);
select get_disk_page (15);
select get_disk_page (10);
*/





