drop database if exists db2;
create database db2;

\c db2

drop table if exists  bufferpool cascade;
create table bufferpool(
		nro_frame	int,
		free		boolean,
		dirty		boolean,
		nro_disk_page	int,
		last_touch	timestamp
);

insert into bufferpool values(1, true, false, 0, clock_timestamp());
insert into bufferpool values(2, true, false, 0, clock_timestamp());
insert into bufferpool values(3, true, false, 0, clock_timestamp());
insert into bufferpool values(4, true, false, 0, clock_timestamp());


