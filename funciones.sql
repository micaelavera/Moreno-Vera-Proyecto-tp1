/*Debe retornar el nro_frame donde se encuentra la pagina de
disco solicitada. Está funcion debe:

-Implementar el algoritmo de lectura de disco o de buffer pool visto en clase.
-Cuando la funcion debe elegir un frame para liberar, debe invocar a la funcion
pick_frame_LRU o pick_frame_MRU o pick_frame139
-La funcion (mediante la clausula RAISE) debe imprimir un mensaje indicando si leyo
de disco o leyo del buffer pool la pagina solicitada, y si desalojo alguna pagina, cual
fue.
*/

create or replace function get_disk_page(p_nro_page integer) returns int as $$
declare 
	fr integer;
begin
	fr=get_frame_by_page(p_nro_page); --el nro de frame de esa pagina
	if fr = 0 then 
		fr=get_page_from_disk(p_nro_page);
	else 
		RAISE NOTICE 'Acceso a buffer - Página: % , Frame: %', p_nro_page, fr;
   
	end if;
    return fr; 	
end;
$$language plpgsql;


create or replace function get_frame_by_page(p_nro_page integer) returns int as $$
declare 
	fr integer;
begin
	select nro_frame into fr
	from bufferpool
	where nro_disk_page = p_nro_page;

	if not found then
		return 0;
	else
		update bufferpool 
		set last_touch = clock_timestamp()
		where nro_disk_page = p_nro_page;
        RAISE NOTICE 'Acceso a disco sin reemplazo - Página:% , Frame:%', p_nro_page, fr; 
	end if;
    return fr;
end;
$$language plpgsql;


create or replace function get_page_from_disk(p_nro_page integer) returns int as $$
declare
	fr integer;
    pag_desalojada integer;
begin  
	fr=get_free_frame(); --elige un buffer libre

    select nro_disk_page into pag_desalojada from bufferpool where nro_frame = fr;
	RAISE NOTICE 'Acceso a disco con reemplazo - Página: % , Frame: % , Pagina desalojada: %' , p_nro_page, fr, pag_desalojada; --Faltaria la pagina desalojada
	--read_pag_from_disk(fr,nro_page());
	update bufferpool 
	set free = false, dirty = false, nro_disk_page = p_nro_page, last_touch = clock_timestamp()
	where nro_frame = fr;
	return fr;
end;
$$language plpgsql;


create or replace function get_free_frame() returns int as $$
declare
	fr integer;
begin
	select nro_frame into fr
	from bufferpool
	where free = true
	order by nro_frame
	limit 1;
	
	if not found then
	fr= pick_frame_LRU(); -- Obtenemos el frame que se debe desalojar
    --fr= pick_frame_MRU(); 
    --fr= pick_frame_139();
	end if;
	return fr;
end;
$$language plpgsql;


/*​ Debe retornar el numero de frame que se debe desalojar segun LRU*/
create or replace function pick_frame_LRU() returns int as $$ 
declare 
	fr integer;
begin			
	select nro_frame into fr 
    from bufferpool 
   	where  last_touch = (select min(last_touch) from bufferpool)
	limit 1;
	return fr;
end;
$$language plpgsql;

/*Debe retornar el numero de frame que se debe desalojar segun MRU*/
create or replace function pick_frame_MRU() returns int as $$
declare 
	fr integer;
begin
	select nro_frame into fr
	from bufferpool 
    where  last_touch = (select max(last_touch) from bufferpool) 
	limit 1;
	return fr;
end;
$$language plpgsql;

/*​ Debe retornar el numero de frame que se debe desalojar segun LRU,
pero antes de cada solicitud se debe verificar si las ultimas N solicitudes fueron
secuenciales (nros de pagina contiguos). Si hubo N secuenciales, debe retornar el numero
de frame segun MRU, y poner en cero el contador de secuenciales. N es un porcentaje de
la cantidad de buffers en el pool (por ejemplo N=50%)*/

create or replace function pick_frame_139() returns int as $$
declare
    fr integer;
    n_secuenciales integer;
    frames record;
    ultimo record;
    anteultimo record;
begin
    select count(nro_frame) as cantidad into frames from bufferpool; --cantidad de frames
    n_secuenciales:=0;
  
    fr = pick_frame_LRU();
    select nro_disk_page into ultimo from bufferpool order by last_touch desc limit 1;
    select nro_disk_page into anteultimo from bufferpool order by last_touch desc limit 1 offset 1;
       
    if anteultimo.nro_disk_page + 1 = ultimo.nro_disk_page then
       n_secuenciales= n_secuenciales + 2;  
           if n_secuenciales = frames.cantidad/2 then --50% de buffers
             fr = pick_frame_MRU();
             n_secuenciales:=0;
           end if;
    end if;
return fr;
end;
$$language plpgsql;
/*
c) Se deben crear 3 trazas de paginas a leer de disco, y crear un script para ejecutar la
funcion get_disk_page() varias veces con todas las trazas de paginas a leer de disco. Por
ejemplo:
select get_disk_page (100);
select get_disk_page (101);
select get_disk_page (102);
select get_disk_page (103);
select get_disk_page (101);
select get_disk_page (102);
select get_disk_page (105);
select get_disk_page (102);select get_disk_page (103);
select get_disk_page (106);
select get_disk_page (102);
Este punto se debe hacer usando MRU , LRU y 139 se deben comparar todos los
resultados. En un caso, MRU debe ser mejor, en otro LRU debe ser mejor y en otro 139
debe ser mejor.
*/
